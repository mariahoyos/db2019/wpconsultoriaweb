=== Milestone Lite ===
Contributors: gracethemes
Tags: blog,two-columns,right-sidebar,custom-logo,full-width-template,footer-widgets,custom-colors,custom-header,featured-images,editor-style,custom-background,custom-menu,threaded-comments,theme-options,translation-ready
Requires at least: WordPress 4.8
Tested up to: WordPress 5.2.2
Requires PHP:  7.2
Tested up to: 5.2.2
Stable tag: 1.0.3
License: GNU General Public License version 2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html


== Description ==
Milestone lite is a Powerful, professional, elegant, flexible Free Responsive Simple WordPress Theme. It is best fit for all kind of Corporate, Professional, medical and multipurpose business. It is user friendly customizer options and Compatible in WordPress Latest Version. also Compatible with WooCommerce, Nextgen gallery ,Contact Form 7 and many WordPress popular plugins.

== Theme License & Copyright ==
*Milestone lite WordPress Theme, Copyright 2017 Grace Themes
* Milestone lite is distributed under the terms of the GNU GPL

== Changelog ==

= 1.0.1 =
* Intial version release

= 1.0.2 =
* theme updated and fixed issues as per reviewer pointed out.

= 1.0.3 =
*Theme updated as per WordPress New Guideline


== Resources ==

Theme is Built using the following resource bundles.

1 - All js that have been used are within folder /js of theme.
2 - jQuery Nivo Slider
	Copyright 2012, Dev7studios, under MIT license
	http://nivo.dev7studios.com


3 - Montserrat :https://www.google.com/fonts/specimen/Montserrat
	License: Distributed under the terms of the Apache License, version 2.0 		
	http://www.apache.org/licenses/LICENSE-2.0.html


4 - Images used from Pixabay.
    Pixabay provides images under CC0 license 
    (https://creativecommons.org/about/cc0)

    Images:	

    Screenshot: 
       https://pixabay.com/en/woman-person-business-professional-801872/
	
5 - Font-Awesome
	Font Awesome 4.7.0 by @davegandy - http://fontawesome.io - @fontawesome
 	License - http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)

6-  milestone-lite/customize-pro	

	Customize Pro code based on Justintadlock�s Customizer-Pro 
	Source Code URL : https://github.com/justintadlock/trt-customizer-pro			
	License : http://www.gnu.org/licenses/gpl-2.0.html
	Copyright 2016, Justin Tadlock	justintadlock.com
	

For any help you can mail us at support@gracethemes.com