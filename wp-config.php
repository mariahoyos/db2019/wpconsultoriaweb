<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'consultoriaweb' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '8YJ`.m9?.evj|)O73q.jq}#P8{EZMDv8(?ZUmc6QrkQ|c`?2,Uk@k]BtNX#@NuR,' );
define( 'SECURE_AUTH_KEY',  'vKc+Bm^6R2_e*(S=bM~W/n!dV901@P+1l5&=v+5cU*uFA[EEaR9twFMXR/lkKZCg' );
define( 'LOGGED_IN_KEY',    'D>5MSmO2%)ck2<jqIA#R%mvGS7dL,Dq~Qrcs(b241tXJ@4KDkj/M^s.pR?l-]M6h' );
define( 'NONCE_KEY',        'm6.M.%iv9U~YV~,mP#QhQ_nCaQBChCS00)@o3@~eSL=qNI`EMa)iGztooD/#N.j>' );
define( 'AUTH_SALT',        'x>b=)3j@Ql/#$0PR$ubYIV6-pl+J;;v6+syN4VU]dHzP@pOu/g6VXnleads2-l`A' );
define( 'SECURE_AUTH_SALT', 'w5{~k[j8t*D9En.PeIXQTg@{NlNx/:? JAL<fq*D^Pq6V.gz7T_6yK%zy=md?`P:' );
define( 'LOGGED_IN_SALT',   '$CI)H?jzv;g$`pyGsZdWmZ8j6vbK<>ZxtrF#9N%QufoI>m*~fJ7nV2==>G}66fe0' );
define( 'NONCE_SALT',       'L~%0m#9vp>g)kh^:6M_v[gjB-q$@2!o~PzA+Aqpy_BnxD<aG-07wZyM{mFXS-B[7' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
